import './bootstrap';

import Vue from 'vue';

import App from './App';
import router from './router';
import store from './store';

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));
Vue.config.productionTip = false;


/* eslint-disable no-new */
window.vue = new Vue({
  components: { App },
  router,
  store,
  template: '<App/>',
}).$mount('#app');

window.mainState = window.vue.$store.state.main
window.localState = window.vue.$store.state.local
