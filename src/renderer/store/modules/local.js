import cards from '../../cards.js';

const initialState = {
  username: '',
  joinedUsers: [],
  myCards: [],
  centerCards: [],
  isAskIhale: false,
  isAskCardType: false,
  oncedenKozOynandi: false,
  userNameAlreadyExists: false,
  isShowRoundWinner: false,
  roundWinnerUserName: '',
  elCount: 0,
  currentElCount: 0,
  gameCompleted: false,
  scoreTable: [],
};

const mutations = {
  c_resetGame(state) {
    state.username = '';
    state.joinedUsers = [];
    state.myCards = [];
    state.centerCards = [];
    state.isAskIhale = false;
    state.isAskCardType = false;
    state.oncedenKozOynandi = false;
    state.userNameAlreadyExists = false;
    state.isShowRoundWinner = false;
    state.roundWinnerUserName = '';
    state.elCount = 0;
    state.currentElCount = 0;
    state.gameCompleted = false;
    state.scoreTable = [];
  },
  c_resetEl(state) {
    state.myCards = [];
    state.centerCards = [];
    state.isAskIhale = false;
    state.isAskCardType = false;
    state.oncedenKozOynandi = false;
    state.userNameAlreadyExists = false;
    state.isShowRoundWinner = false;
    state.roundWinnerUserName = '';
  },
  setRoundWinner(state, data) {
    state.isShowRoundWinner = data.isShow;
    state.roundWinnerUserName = data.username;
  },
  setUsername(state, username) {
    state.username = username;
  },
  c_setJoinedUsers(state, joinedUsers) {
    state.joinedUsers = joinedUsers;
  },
  setCenterCards(state, centerCards) {
    state.centerCards = centerCards;
  },
  setCards(state, cards) {
    state.myCards = cards;
  },
  setIsAskIhale(state, status) {
    state.isAskIhale = status;  
  },
  setIsAskCardType(state, status) {
    state.isAskCardType = status;
  },
  removeMyCard(state, card) {
    const index = state.myCards.findIndex(_card => _card.value == card.value);
    state.myCards.splice(index, 1);
  },
  setOncedenKozOynandi(state, value) {
    state.oncedenKozOynandi = value;
  },
  setUserNameAlreadyExists(state, value) {
    state.userNameAlreadyExists = value;
  }
};

export default {
  state: initialState,
  mutations,
};

// username kontrol
// isimleri doğru göster herkes kendini önde görsün

// koz göster
// sıra sende göster
// eli kimin aldığını göster
// 13 seçince ihale sorma kimseye