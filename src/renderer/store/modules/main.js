import cards from '../../cards.js';

const initialState = {
  cards: JSON.parse(JSON.stringify(cards.shuffle())),
  centerCards: [],
  joinedUsers: [],
  oncedenKozOynandi: false,
  cardCount: 4 * 2,
  elCount: 0,
  currentElCount: 0,
  scoreTable: {
    // "ahmet": { "scores": [1,2,3] },
    // "ahmet": { "scores": [1,2,3] },
    // "ahmet": { "scores": [1,2,3] },
    // "ahmet": { "scores": [1,2,3] },
  },
};

const mutations = {
  decrementCardCount(state) {
    state.cardCount = state.cardCount - 1;
  },
  s_resetGame(state) {
    state.cards = JSON.parse(JSON.stringify(cards.shuffle()));
    state.centerCards = [];
    state.joinedUsers = [];
    state.oncedenKozOynandi = false;
    state.cardCount = 4*2;
    state.scoreTable = {};
    state.elCount = 0;
    state.currentElCount = 0;
  },
  s_resetEl(state) {
    state.cards = JSON.parse(JSON.stringify(cards.shuffle()));
    state.centerCards = [];
    state.oncedenKozOynandi = false;
    state.cardCount = 4*2;
  },
  addUser(state, username) {
    state.joinedUsers.push({ username, puan: 0, isMyOrder: false });
    state.scoreTable[username] = { scores: [] };
  },
  addCardToCenter(state, card) {
    if (state.centerCards.length < 4) {
      state.centerCards.push(card);
    }
  },
  setIhaleValue(state, data) {
    const index = state.joinedUsers.findIndex(item => item.username == data.username);
    state.joinedUsers[index].ihale = data.value;
  },
  setJoinedUsers(state, data) {
    state.joinedUsers = data;
  },
  setCardType(state, data) {
    const index = state.joinedUsers.findIndex(user => user.ihaleBende);
    state.joinedUsers[index].cardType = data.value;
  },
  setOncedenKozOynandi(state, value) {
    state.oncedenKozOynandi = value;
  },
  clearCenterCards(state) {
    state.centerCards = [];
  }
};

export default {
  state: initialState,
  mutations,
};
