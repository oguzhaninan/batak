const ip = require('ip');
const server = require('http').createServer();
const io = require('socket.io')(server);

export default class ServerManager {
  static createServer() {
    io.on('connection', (client) => {
      
      client.on('resetGame', () => {
        io.sockets.emit('resetGame');
        server.close(() => console.log('socket closed'));
      });

      // kullanıcı ekleniyor
      client.on('userJoin', (username) => {
        if (window.mainState.joinedUsers.findIndex(_user => _user.username == username) != -1) {
          io.sockets.emit('userNameAlreadyExists');
        } else {
          // 4 den az kullanıcı var ise oyuncu ekle
          if (window.mainState.joinedUsers.length < 4) {
            window.vue.$store.commit('addUser', username);
            io.sockets.emit('setJoinedUsers', window.mainState.joinedUsers);
          }

          this.distributeCards();
        }
      });

      client.on('ihaleAnswered', (data) => {
        window.vue.$store.commit('setIhaleValue', data);
        io.sockets.emit('setJoinedUsers', window.mainState.joinedUsers);

        // birisi 13 seçmiş kimseye sormaya gerek yok.
        if (data.value == 13) {
          const jo = window.mainState.joinedUsers.map(user => {
            user.ihale = user.username != data.username ? -1 : 13;
            return user;
          });
          window.vue.$store.commit('setJoinedUsers', jo);
          io.sockets.emit('setJoinedUsers', jo)
        }

        // ihale seçmeyen varsa
        if (!window.mainState.joinedUsers.every(item => item.ihale !== undefined)) {
          let index = window.mainState.joinedUsers.findIndex(item => item.username == data.username);
          index = (index + 1) >= window.mainState.joinedUsers.length ? 0 : index + 1;
          const gamer = window.mainState.joinedUsers[index]; // kartları dağıltanın yanındaki kişi
          io.sockets.emit('askIhale', gamer);
        }
        // herkes ihale seçmiş
        else {
          // herkes pas demiş
          if (window.mainState.joinedUsers.every(user => user.ihale === -1)) {
            // serverin yanındakine ihale 4 olarak kaldı
            const jo = window.mainState.joinedUsers.map((user, i) => {
              user.ihaleBende = (i == 1);
              user.ihale = (i == 1) ? 4 : -1;
              return user;
            });
            window.vue.$store.commit('setJoinedUsers', jo);
          } else {
            let maxIhale = Math.max(...window.mainState.joinedUsers.map(user => user.ihale));
            let index = window.mainState.joinedUsers.findIndex(user => user.ihale == maxIhale);
            const jo = window.mainState.joinedUsers.map((user, i) => {
              user.ihaleBende = (i == index);
              return user;
            });
            window.vue.$store.commit('setJoinedUsers', jo);
          }
          io.sockets.emit('setJoinedUsers', window.mainState.joinedUsers)
          io.sockets.emit('askCardType', window.mainState.joinedUsers.find(user => user.ihaleBende == true))
        }
      });

      // ihale kime kaldıysa kart seçimi yaptı
      client.on('cardTypeAnswered', (data) => {
        window.vue.$store.commit('setCardType', data);

        // ihale kimdeyse sıra ona verilir ve oyun başlar
        let index = window.mainState.joinedUsers.findIndex(user => user.ihaleBende);
        const jo = window.mainState.joinedUsers.map((user, i) => {
          user.isMyOrder = (i == index);
          return user;
        });
        window.vue.$store.commit('setJoinedUsers', jo);
        io.sockets.emit('setJoinedUsers', jo);
      });

      // masaya kart atıldığında
      client.on('centerCard', (card) => {
        window.vue.$store.commit('addCardToCenter', card);
        io.sockets.emit('setCenterCards', window.mainState.centerCards);

        window.vue.$store.commit('decrementCardCount');

        // eğer son kart geldiyse kazananı belirle
        const centerCards = window.mainState.centerCards
        const joinedUsers = window.mainState.joinedUsers
        if (centerCards.length == 4) {
          const kozType = joinedUsers.filter(_user => _user.ihaleBende)[0].cardType;

          const masadakiKozlar = centerCards.filter(_card => _card.type == kozType);
          const kozVar = masadakiKozlar.length > 0;
          const masadakiEnBuyukKoz = Math.max(...masadakiKozlar.map(_card => _card.rank));
          
          const ilkKartTipindekiler = centerCards.filter(_card => _card.type == centerCards[0].type);
          const masadakiEnBuyukIlkKart = Math.max(...ilkKartTipindekiler.map(_card => _card.rank));

          if (kozVar) {
            var _username = masadakiKozlar.find(_card => _card.rank == masadakiEnBuyukKoz).username;
          } else {
            var _username = ilkKartTipindekiler.find(_card => _card.rank == masadakiEnBuyukIlkKart).username;
          }

          const userIndex = joinedUsers.findIndex(_user => _user.username == _username);
          joinedUsers[userIndex].puan++;
          const jo = joinedUsers.map((user, i) => {
            user.isMyOrder = (i == userIndex);
            return user;
          });
          window.vue.$store.commit('setJoinedUsers', jo);
          io.sockets.emit('setJoinedUsers', jo);

          io.sockets.emit('setRoundWinner', jo[userIndex].username);

          window.vue.$store.commit('clearCenterCards');
          io.sockets.emit('setCenterCards', window.mainState.centerCards);
          
          // önceden oynandi mi?
          if (! window.mainState.oncedenKozOynandi) {
            window.vue.$store.commit('setOncedenKozOynandi', kozVar);
            io.sockets.emit('setOncedenKozOynandi', kozVar);
          }
        } else {
          let index = window.mainState.joinedUsers.findIndex(item => item.username == card.username);
          index = (index + 1) >= window.mainState.joinedUsers.length ? 0 : index + 1;
          const jo = window.mainState.joinedUsers.map((user, i) => {
            user.isMyOrder = (i == index);
            return user;
          });
          window.vue.$store.commit('setJoinedUsers', jo);
          io.sockets.emit('setJoinedUsers', jo);
        }

        // kart kalmadıysa oyun bitmiştir
        if (window.mainState.cardCount == 0) {
          const scoreTable = window.mainState.scoreTable

          // puanları belirledik
          const ihaleliUser = window.mainState.joinedUsers.find(_user => _user.ihaleBende);
          const ihaleliKazandi = ihaleliUser.puan >= ihaleliUser.ihale;
          if (ihaleliKazandi) {
            scoreTable[ihaleliUser.username].scores.push(ihaleliUser.puan);
          } else {
            scoreTable[ihaleliUser.username].scores.push(-ihaleliUser.ihale);
          }
          window.mainState.joinedUsers.forEach(_user => {
            if (_user.username != ihaleliUser.username) {
              if (_user.puan != 0) {
                scoreTable[_user.username].scores.push(_user.puan)
              } else {
                scoreTable[_user.username].scores.push(-_user.puan)
              }
            }
          });

          // el sayısını herkesde eşitledik
          window.mainState.currentElCount = window.mainState.currentElCount + 1
          io.sockets.emit('setElCount', parseInt(window.mainState.currentElCount));

          // parti bitmediyse
          if (window.mainState.currentElCount != window.mainState.elCount) {
            // tekrar kart dağıt
            const jo = window.mainState.joinedUsers.map((user, i) => ({ username: user.username, puan: 0, isMyOrder: false }));
            window.vue.$store.commit('setJoinedUsers', jo);
            io.sockets.emit('setJoinedUsers', jo);

            window.vue.$store.commit('s_resetEl');
            io.sockets.emit('resetEl');
            this.distributeCards();
          } else {
            io.sockets.emit('gameCompleted', window.mainState.scoreTable);
          }
        }
      });

      client.on('setElCount', (count) => {
        window.mainState.elCount = parseInt(count);
        io.sockets.emit('setElCount', parseInt(count));
      })

      client.on('error', (err) => {
        console.log('received error from client:', client.id);
      });
    });

    server.listen(3000, ip.address(), (err) => {
      if (err) throw err;
      console.log('Listening on port 3000');
    });

  }

  static distributeCards() {
    // kart dağıt
    if (window.mainState.joinedUsers.length == 4) {
      window.mainState.joinedUsers.forEach((user, i) => {
        let cards = mainState.cards.splice(0, 13);
        io.sockets.emit('distributeCard', {user,cards});
      });

      // ihale sor
      setTimeout(() => {
        const gamer = window.mainState.joinedUsers[1]; // kartları dağıltanın yanındaki kişi
        io.sockets.emit('askIhale', gamer);
      }, 200);
    }
  }
}