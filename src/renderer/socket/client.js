const io = require('socket.io-client');

const socket = io('http://192.168.1.21:3000', { autoConnect: false });

export default class ClientManager {

  static connectToGame() {
    socket.open();
    
    socket.on('connect_error', function() {
      window.vue.$router.replace({ name: "main-menu" , params: { error: "Katılabileceğiniz masa yok." }});
      socket.disconnect();
    });

    socket.on('connect', function() {
      console.log("....connect");
      socket.on('setOncedenKozOynandi', (value) => {
        window.vue.$store.commit('setOncedenKozOynandi', value);
      });
  
      socket.on('setJoinedUsers', (joinedUser) => {
        window.vue.$store.commit('c_setJoinedUsers', joinedUser);
      });
      
      socket.on('setCenterCards', (centerCards) => {
        window.vue.$store.commit('setCenterCards', centerCards);
      });
  
      socket.on('distributeCard', (data) => {
        if (window.localState.username == data.user.username) {
          window.vue.$store.commit('setCards', data.cards);
        }
      });
  
      socket.on('askIhale', (gamer) => {
        if (window.localState.username == gamer.username) { // bana mı sorulmuş
          window.vue.$store.commit('setIsAskIhale', true);
        }
      });
  
      socket.on('askCardType', (gamer) => {
        if (window.localState.username == gamer.username) { // bana mı sorulmuş
          window.vue.$store.commit('setIsAskCardType', true);
        }
      });
  
      socket.on('userNameAlreadyExists', () => {
        window.vue.$store.commit('setUserNameAlreadyExists', true);
      })
  
      socket.on('resetGame', () => {
        window.vue.$store.commit('c_resetGame');
        window.vue.$router.replace({name: 'main-menu'})
      });
  
      socket.on('resetEl', () => {
        window.vue.$store.commit('c_resetEl');
      });
  
      socket.on('setElCount', (count) => {
        window.localState.elCount = parseInt(count);
      });
  
      socket.on('gameCompleted', (scoreTable) => {
        window.localState.gameCompleted = true;
        window.localState.scoreTable = scoreTable;
      });
  
      socket.on('setRoundWinner', (username) => {
        window.vue.$store.commit('setRoundWinner', {isShow: true, username});
        setTimeout(() => window.vue.$store.commit('setRoundWinner', false, ''), 4*1000);
      });
    });
  }

  static setElCount(count) {
    socket.emit('setElCount', parseInt(count));  
  }

  static addCardToCenter(card) {
    socket.emit('centerCard', card);  
  }

  static setIhaleValue(username, value) {
    socket.emit('ihaleAnswered', { username, value });
  }

  static setIhaleCardType(username, value) {
    socket.emit('cardTypeAnswered', { username, value });
  }

  static sendUsername(username) {
    socket.emit('userJoin', username);
  }

  static resetGame() {
    socket.emit('resetGame');
  }
}


// function registerHandler(onMessageReceived) {
//   socket.on('message', onMessageReceived)
// }

// function unregisterHandler() {
//   socket.off('message')
// }

// join("alihasan", null);
