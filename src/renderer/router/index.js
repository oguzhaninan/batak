import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'splash-screen',
      component: require('@/components/SplashScreen').default,
    },
    {
      path: '/main-menu',
      name: 'main-menu',
      component: require('@/components/MainMenu').default,
    },
    {
      path: '/game',
      name: 'game-pane',
      component: require('@/components/GamePane').default,
    },
    {
      path: '*',
      redirect: '/game',
    },
  ],
});
